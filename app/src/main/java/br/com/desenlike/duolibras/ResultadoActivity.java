package br.com.desenlike.duolibras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.List;

public class ResultadoActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Intent intent = getIntent();
        String acertosst = intent.getStringExtra("acertos");
        String errosst = intent.getStringExtra("erros");
        String tempo = intent.getStringExtra("tempo");
        tempo = tempo.substring(tempo.lastIndexOf(" ") + 1);
        String nome = intent.getStringExtra("nome");
        String categoria = intent.getStringExtra("categoria");
        int acertos = Integer.parseInt(acertosst);
        int erros = Integer.parseInt(errosst);


        TextView acertosTV = (TextView) findViewById(R.id.resultadoacertos);
        TextView errosTV = (TextView) findViewById(R.id.resultadoerros);
        TextView percentualTV = (TextView) findViewById(R.id.percentual);
        TextView tempoTV = (TextView) findViewById(R.id.txtempo);
        TextView nomeTV = (TextView) findViewById(R.id.nome);
        TextView categoriaTV = (TextView) findViewById(R.id.categoria);

        if(acertos > 0) {
            int total = acertos + erros;
            int percentual = (acertos * 100) / total;
            percentualTV.setText(percentual + "");
        }else{
            percentualTV.setText("0");
        }

        acertosTV.setText(acertos+"");
        errosTV.setText(erros+"");
        tempoTV.setText(tempo);
        nomeTV.setText(nome);
        categoriaTV.setText(categoria);

        //grava dados no banco
        HistoricoDAO dao = new HistoricoDAO(getApplicationContext());
        HistoricoVO vo = new HistoricoVO();
        vo.setNome(nome);
        vo.setAcertos(acertos);
        vo.setErros(erros);
        vo.setTempo(tempo);
        vo.setCategoria(categoria);
        //monta data atual
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("d-M-yyyy H:m:s");
        String dateString = sdf.format(date);
        vo.setDatahora(dateString);
        try {
            dao.insert(vo);
        }catch (Exception e){
            System.out.println("Erro: "+e.getMessage());
        }


        final Button finalizar = (Button) findViewById(R.id.finalizar);
        finalizar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PerguntasActivity.getInstance().finish();
                finish();
            }
        });

        List<HistoricoVO> lista = dao.list();
        int duracao = Toast.LENGTH_LONG;
        Toast t = Toast.makeText(this, "Quantidade:"+ lista.size() , duracao);
        //t.show();
    }
    @Override
    public void onBackPressed()
    {
        PerguntasActivity.getInstance().finish();
        finish();
    }
}
