package br.com.desenlike.duolibras;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class PerguntasActivity extends AppCompatActivity {

    int correto;
    LinearLayout listaPerguntasLL = null;
    TextView perguntaTV = null;
    TextView respacertosTV = null;
    TextView resperrosTV = null;
    List<PerguntasVO> listaPerguntas = null;
    int indice = 0;
    Chronometer m_chronometer;
    static PerguntasActivity activity;
    String nome;
    String categoria;
    CategoriasVO categoriaSelecionada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perguntas);
        activity = this;
        long idpergunta;
        int cont = 0;
        m_chronometer = (Chronometer) findViewById(R.id.chronometer);
        m_chronometer.start();

        Intent intent = getIntent();
        nome = intent.getStringExtra("nome");
        categoria = intent.getStringExtra("categoria");

        CategoriasDAO categoriasdao = new CategoriasDAO(getApplicationContext());

        categoriaSelecionada = categoriasdao.getCategoriaByName(categoria);


        //cadastra as perguntas e respostas
        PerguntasDAO perguntasdao = new PerguntasDAO(getApplicationContext());
        listaPerguntas = perguntasdao.list(categoriaSelecionada.getId());
        RespostasDAO respostasdao = new RespostasDAO(getApplicationContext());

        if(listaPerguntas.size() == 0){

            //pergunta 1
            PerguntasVO pergunta1 = new PerguntasVO();
            pergunta1.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta1.setPergunta("Como se diz ÁGUA em Libras?");
            idpergunta = perguntasdao.insert(pergunta1);

            RespostasVO resposta1pergunta1 = new RespostasVO();
            resposta1pergunta1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta1.setImagemresposta(R.drawable.limpo);
            resposta1pergunta1.setCorreta("nao");
            respostasdao.insert(resposta1pergunta1);

            RespostasVO resposta2pergunta1 = new RespostasVO();
            resposta2pergunta1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta1.setImagemresposta(R.drawable.beber);
            resposta2pergunta1.setCorreta("nao");
            respostasdao.insert(resposta2pergunta1);

            RespostasVO resposta3pergunta1 = new RespostasVO();
            resposta3pergunta1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta1.setImagemresposta(R.drawable.agua);
            resposta3pergunta1.setCorreta("sim");
            respostasdao.insert(resposta3pergunta1);

            //pergunta 1.1
            PerguntasVO pergunta1_1 = new PerguntasVO();
            pergunta1_1.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta1_1.setPergunta("Como se diz COPO em Libras?");
            idpergunta = perguntasdao.insert(pergunta1_1);

            RespostasVO resposta1pergunta1_1 = new RespostasVO();
            resposta1pergunta1_1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta1_1.setImagemresposta(R.drawable.pessoas);
            resposta1pergunta1_1.setCorreta("nao");
            respostasdao.insert(resposta1pergunta1_1);

            RespostasVO resposta2pergunta1_1 = new RespostasVO();
            resposta2pergunta1_1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta1_1.setImagemresposta(R.drawable.copo);
            resposta2pergunta1_1.setCorreta("sim");
            respostasdao.insert(resposta2pergunta1_1);

            RespostasVO resposta3pergunta1_1 = new RespostasVO();
            resposta3pergunta1_1.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta1_1.setImagemresposta(R.drawable.beber);
            resposta3pergunta1_1.setCorreta("nao");
            respostasdao.insert(resposta3pergunta1_1);

            //pergunta 1.2
            PerguntasVO pergunta1_2 = new PerguntasVO();
            pergunta1_2.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta1_2.setPergunta("Como se diz FACA em Libras?");
            idpergunta = perguntasdao.insert(pergunta1_2);

            RespostasVO resposta1pergunta1_2 = new RespostasVO();
            resposta1pergunta1_2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta1_2.setImagemresposta(R.drawable.faca);
            resposta1pergunta1_2.setCorreta("sim");
            respostasdao.insert(resposta1pergunta1_2);

            RespostasVO resposta2pergunta1_2 = new RespostasVO();
            resposta2pergunta1_2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta1_2.setImagemresposta(R.drawable.feijao);
            resposta2pergunta1_2.setCorreta("nao");
            respostasdao.insert(resposta2pergunta1_2);

            RespostasVO resposta3pergunta1_2 = new RespostasVO();
            resposta3pergunta1_2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta1_2.setImagemresposta(R.drawable.dormir);
            resposta3pergunta1_2.setCorreta("nao");
            respostasdao.insert(resposta3pergunta1_2);

            //pergunta 2
            PerguntasVO pergunta2 = new PerguntasVO();
            pergunta2.setIdcategoria(categoriasdao.getCategoriaByName("Saudações").getId());
            pergunta2.setPergunta("Como se diz BOA TARDE em Libras?");
            idpergunta = perguntasdao.insert(pergunta2);

            RespostasVO resposta1pergunta2 = new RespostasVO();
            resposta1pergunta2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta2.setImagemresposta(R.drawable.boatarde);
            resposta1pergunta2.setCorreta("sim");
            respostasdao.insert(resposta1pergunta2);

            RespostasVO resposta2pergunta2 = new RespostasVO();
            resposta2pergunta2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta2.setImagemresposta(R.drawable.bomdia);
            resposta2pergunta2.setCorreta("nao");
            respostasdao.insert(resposta2pergunta2);

            RespostasVO resposta3pergunta2 = new RespostasVO();
            resposta3pergunta2.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta2.setImagemresposta(R.drawable.boanoite);
            resposta3pergunta2.setCorreta("nao");
            respostasdao.insert(resposta3pergunta2);

            //pergunta 21
            PerguntasVO pergunta21 = new PerguntasVO();
            pergunta21.setIdcategoria(categoriasdao.getCategoriaByName("Saudações").getId());
            pergunta21.setPergunta("Como se diz BOA NOITE em Libras?");
            idpergunta = perguntasdao.insert(pergunta21);

            RespostasVO resposta1pergunta21 = new RespostasVO();
            resposta1pergunta21.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta21.setImagemresposta(R.drawable.banheiro);
            resposta1pergunta21.setCorreta("nao");
            respostasdao.insert(resposta1pergunta21);

            RespostasVO resposta2pergunta21 = new RespostasVO();
            resposta2pergunta21.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta21.setImagemresposta(R.drawable.aranha);
            resposta2pergunta21.setCorreta("nao");
            respostasdao.insert(resposta2pergunta21);

            RespostasVO resposta3pergunta21 = new RespostasVO();
            resposta3pergunta21.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta21.setImagemresposta(R.drawable.boanoite);
            resposta3pergunta21.setCorreta("sim");
            respostasdao.insert(resposta3pergunta21);

            //pergunta 22
            PerguntasVO pergunta22 = new PerguntasVO();
            pergunta22.setIdcategoria(categoriasdao.getCategoriaByName("Saudações").getId());
            pergunta22.setPergunta("Como se diz BOM DIA em Libras?");
            idpergunta = perguntasdao.insert(pergunta22);

            RespostasVO resposta1pergunta22 = new RespostasVO();
            resposta1pergunta22.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta22.setImagemresposta(R.drawable.bomdia);
            resposta1pergunta22.setCorreta("sim");
            respostasdao.insert(resposta1pergunta22);

            RespostasVO resposta2pergunta22 = new RespostasVO();
            resposta2pergunta22.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta22.setImagemresposta(R.drawable.carne);
            resposta2pergunta22.setCorreta("nao");
            respostasdao.insert(resposta2pergunta22);

            RespostasVO resposta3pergunta22 = new RespostasVO();
            resposta3pergunta22.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta22.setImagemresposta(R.drawable.aviao);
            resposta3pergunta22.setCorreta("nao");
            respostasdao.insert(resposta3pergunta22);

            //pergunta 3
            PerguntasVO pergunta3 = new PerguntasVO();
            pergunta3.setIdcategoria(categoriasdao.getCategoriaByName("Verbos").getId());
            pergunta3.setPergunta("Como se diz ANDAR em Libras?");
            idpergunta = perguntasdao.insert(pergunta3);

            RespostasVO resposta1pergunta3 = new RespostasVO();
            resposta1pergunta3.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta3.setImagemresposta(R.drawable.andar);
            resposta1pergunta3.setCorreta("sim");
            respostasdao.insert(resposta1pergunta3);

            RespostasVO resposta2pergunta3 = new RespostasVO();
            resposta2pergunta3.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta3.setImagemresposta(R.drawable.aranha);
            resposta2pergunta3.setCorreta("nao");
            respostasdao.insert(resposta2pergunta3);

            RespostasVO resposta3pergunta3 = new RespostasVO();
            resposta3pergunta3.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta3.setImagemresposta(R.drawable.peixe);
            resposta3pergunta3.setCorreta("nao");
            respostasdao.insert(resposta3pergunta3);

            //pergunta 31
            PerguntasVO pergunta31 = new PerguntasVO();
            pergunta31.setIdcategoria(categoriasdao.getCategoriaByName("Verbos").getId());
            pergunta31.setPergunta("Como se diz BEBER em Libras?");
            idpergunta = perguntasdao.insert(pergunta31);

            RespostasVO resposta1pergunta31 = new RespostasVO();
            resposta1pergunta31.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta31.setImagemresposta(R.drawable.faca);
            resposta1pergunta31.setCorreta("nao");
            respostasdao.insert(resposta1pergunta31);

            RespostasVO resposta2pergunta31 = new RespostasVO();
            resposta2pergunta31.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta31.setImagemresposta(R.drawable.cama);
            resposta2pergunta31.setCorreta("nao");
            respostasdao.insert(resposta2pergunta31);

            RespostasVO resposta3pergunta31 = new RespostasVO();
            resposta3pergunta31.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta31.setImagemresposta(R.drawable.beber);
            resposta3pergunta31.setCorreta("sim");
            respostasdao.insert(resposta3pergunta31);

            //pergunta 32
            PerguntasVO pergunta32 = new PerguntasVO();
            pergunta32.setIdcategoria(categoriasdao.getCategoriaByName("Verbos").getId());
            pergunta32.setPergunta("Como se diz COMER em Libras?");
            idpergunta = perguntasdao.insert(pergunta32);

            RespostasVO resposta1pergunta32 = new RespostasVO();
            resposta1pergunta32.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta32.setImagemresposta(R.drawable.prato);
            resposta1pergunta32.setCorreta("nao");
            respostasdao.insert(resposta1pergunta32);

            RespostasVO resposta2pergunta32 = new RespostasVO();
            resposta2pergunta32.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta32.setImagemresposta(R.drawable.copo);
            resposta2pergunta32.setCorreta("nao");
            respostasdao.insert(resposta2pergunta32);

            RespostasVO resposta3pergunta32 = new RespostasVO();
            resposta3pergunta32.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta32.setImagemresposta(R.drawable.comer);
            resposta3pergunta32.setCorreta("sim");
            respostasdao.insert(resposta3pergunta32);

            //pergunta 4
            PerguntasVO pergunta4 = new PerguntasVO();
            pergunta4.setIdcategoria(categoriasdao.getCategoriaByName("Veículos").getId());
            pergunta4.setPergunta("Selecione o correspondente a AVIÃO:");
            idpergunta = perguntasdao.insert(pergunta4);

            RespostasVO resposta1pergunta4 = new RespostasVO();
            resposta1pergunta4.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta4.setImagemresposta(R.drawable.peixe);
            resposta1pergunta4.setCorreta("nao");
            respostasdao.insert(resposta1pergunta4);

            RespostasVO resposta2pergunta4 = new RespostasVO();
            resposta2pergunta4.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta4.setImagemresposta(R.drawable.navio);
            resposta2pergunta4.setCorreta("nao");
            respostasdao.insert(resposta2pergunta4);

            RespostasVO resposta3pergunta4 = new RespostasVO();
            resposta3pergunta4.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta4.setImagemresposta(R.drawable.aviao);
            resposta3pergunta4.setCorreta("sim");
            respostasdao.insert(resposta3pergunta4);

            //pergunta 5
            PerguntasVO pergunta5 = new PerguntasVO();
            pergunta5.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta5.setPergunta("Como se diz BANHEIRO?");
            idpergunta = perguntasdao.insert(pergunta5);

            RespostasVO resposta1pergunta5 = new RespostasVO();
            resposta1pergunta5.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta5.setImagemresposta(R.drawable.banheiro);
            resposta1pergunta5.setCorreta("sim");
            respostasdao.insert(resposta1pergunta5);

            RespostasVO resposta2pergunta5 = new RespostasVO();
            resposta2pergunta5.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta5.setImagemresposta(R.drawable.faca);
            resposta2pergunta5.setCorreta("nao");
            respostasdao.insert(resposta2pergunta5);

            RespostasVO resposta3pergunta5 = new RespostasVO();
            resposta3pergunta5.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta5.setImagemresposta(R.drawable.limpo);
            resposta3pergunta5.setCorreta("nao");
            respostasdao.insert(resposta3pergunta5);

            //pergunta 6
            PerguntasVO pergunta6 = new PerguntasVO();
            pergunta6.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta6.setPergunta("Como se diz CAMA?");
            idpergunta = perguntasdao.insert(pergunta6);

            RespostasVO resposta1pergunta6 = new RespostasVO();
            resposta1pergunta6.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta6.setImagemresposta(R.drawable.cama);
            resposta1pergunta6.setCorreta("sim");
            respostasdao.insert(resposta1pergunta6);

            RespostasVO resposta2pergunta6 = new RespostasVO();
            resposta2pergunta6.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta6.setImagemresposta(R.drawable.sentado);
            resposta2pergunta6.setCorreta("nao");
            respostasdao.insert(resposta2pergunta6);

            RespostasVO resposta3pergunta6 = new RespostasVO();
            resposta3pergunta6.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta6.setImagemresposta(R.drawable.copo);
            resposta3pergunta6.setCorreta("nao");
            respostasdao.insert(resposta3pergunta6);

            //pergunta 7
            PerguntasVO pergunta7 = new PerguntasVO();
            pergunta7.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta7.setPergunta("Como se diz PRATO?");
            idpergunta = perguntasdao.insert(pergunta7);

            RespostasVO resposta1pergunta7 = new RespostasVO();
            resposta1pergunta7.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta7.setImagemresposta(R.drawable.familia);
            resposta1pergunta7.setCorreta("nao");
            respostasdao.insert(resposta1pergunta7);

            RespostasVO resposta2pergunta7 = new RespostasVO();
            resposta2pergunta7.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta7.setImagemresposta(R.drawable.prato);
            resposta2pergunta7.setCorreta("sim");
            respostasdao.insert(resposta2pergunta7);

            RespostasVO resposta3pergunta7 = new RespostasVO();
            resposta3pergunta7.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta7.setImagemresposta(R.drawable.rua);
            resposta3pergunta7.setCorreta("nao");
            respostasdao.insert(resposta3pergunta7);

            //pergunta 8
            PerguntasVO pergunta8 = new PerguntasVO();
            pergunta8.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta8.setPergunta("Como se diz CASA?");
            idpergunta = perguntasdao.insert(pergunta8);

            RespostasVO resposta1pergunta8 = new RespostasVO();
            resposta1pergunta8.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta8.setImagemresposta(R.drawable.casa);
            resposta1pergunta8.setCorreta("sim");
            respostasdao.insert(resposta1pergunta8);

            RespostasVO resposta2pergunta8 = new RespostasVO();
            resposta2pergunta8.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta8.setImagemresposta(R.drawable.feijao);
            resposta2pergunta8.setCorreta("nao");
            respostasdao.insert(resposta2pergunta8);

            RespostasVO resposta3pergunta8 = new RespostasVO();
            resposta3pergunta8.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta8.setImagemresposta(R.drawable.familia);
            resposta3pergunta8.setCorreta("nao");
            respostasdao.insert(resposta3pergunta8);

            //pergunta 9
            PerguntasVO pergunta9 = new PerguntasVO();
            pergunta9.setIdcategoria(categoriasdao.getCategoriaByName("Veículos").getId());
            pergunta9.setPergunta("Como se diz NAVIO em Libras?");
            idpergunta = perguntasdao.insert(pergunta9);

            RespostasVO resposta1pergunta9 = new RespostasVO();
            resposta1pergunta9.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta9.setImagemresposta(R.drawable.casa);
            resposta1pergunta9.setCorreta("nao");
            respostasdao.insert(resposta1pergunta9);

            RespostasVO resposta2pergunta9 = new RespostasVO();
            resposta2pergunta9.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta9.setImagemresposta(R.drawable.peixe);
            resposta2pergunta9.setCorreta("nao");
            respostasdao.insert(resposta2pergunta9);

            RespostasVO resposta3pergunta9 = new RespostasVO();
            resposta3pergunta9.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta9.setImagemresposta(R.drawable.navio);
            resposta3pergunta9.setCorreta("sim");
            respostasdao.insert(resposta3pergunta9);

            //pergunta 10
            PerguntasVO pergunta10 = new PerguntasVO();
            pergunta10.setIdcategoria(categoriasdao.getCategoriaByName("Animais").getId());
            pergunta10.setPergunta("Como se diz PEIXE em Libras?");
            idpergunta = perguntasdao.insert(pergunta10);

            RespostasVO resposta1pergunta10 = new RespostasVO();
            resposta1pergunta10.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta10.setImagemresposta(R.drawable.aviao);
            resposta1pergunta10.setCorreta("nao");
            respostasdao.insert(resposta1pergunta10);

            RespostasVO resposta2pergunta10 = new RespostasVO();
            resposta2pergunta10.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta10.setImagemresposta(R.drawable.peixe);
            resposta2pergunta10.setCorreta("sim");
            respostasdao.insert(resposta2pergunta10);

            RespostasVO resposta3pergunta10 = new RespostasVO();
            resposta3pergunta10.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta10.setImagemresposta(R.drawable.dormir);
            resposta3pergunta10.setCorreta("nao");
            respostasdao.insert(resposta3pergunta10);

            //pergunta 101
            PerguntasVO pergunta101 = new PerguntasVO();
            pergunta101.setIdcategoria(categoriasdao.getCategoriaByName("Animais").getId());
            pergunta101.setPergunta("Como se diz ARANHA em Libras?");
            idpergunta = perguntasdao.insert(pergunta101);

            RespostasVO resposta1pergunta101 = new RespostasVO();
            resposta1pergunta101.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta101.setImagemresposta(R.drawable.aranha);
            resposta1pergunta101.setCorreta("sim");
            respostasdao.insert(resposta1pergunta101);

            RespostasVO resposta2pergunta101 = new RespostasVO();
            resposta2pergunta101.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta101.setImagemresposta(R.drawable.beber);
            resposta2pergunta101.setCorreta("nao");
            respostasdao.insert(resposta2pergunta101);

            RespostasVO resposta3pergunta101 = new RespostasVO();
            resposta3pergunta101.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta101.setImagemresposta(R.drawable.casa);
            resposta3pergunta101.setCorreta("nao");
            respostasdao.insert(resposta3pergunta101);

            //pergunta 11
            PerguntasVO pergunta11 = new PerguntasVO();
            pergunta11.setIdcategoria(categoriasdao.getCategoriaByName("Verbos").getId());
            pergunta11.setPergunta("Como se diz DORMIR em Libras?");
            idpergunta = perguntasdao.insert(pergunta11);

            RespostasVO resposta1pergunta11 = new RespostasVO();
            resposta1pergunta11.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta11.setImagemresposta(R.drawable.dormir);
            resposta1pergunta11.setCorreta("sim");
            respostasdao.insert(resposta1pergunta11);

            RespostasVO resposta2pergunta11 = new RespostasVO();
            resposta2pergunta11.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta11.setImagemresposta(R.drawable.bomdia);
            resposta2pergunta11.setCorreta("nao");
            respostasdao.insert(resposta2pergunta11);

            RespostasVO resposta3pergunta11 = new RespostasVO();
            resposta3pergunta11.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta11.setImagemresposta(R.drawable.rua);
            resposta3pergunta11.setCorreta("nao");
            respostasdao.insert(resposta3pergunta11);

            //pergunta 12
            PerguntasVO pergunta12 = new PerguntasVO();
            pergunta12.setIdcategoria(categoriasdao.getCategoriaByName("Alimentos").getId());
            pergunta12.setPergunta("Como se diz FEIJÃO?");
            idpergunta = perguntasdao.insert(pergunta12);

            RespostasVO resposta1pergunta12 = new RespostasVO();
            resposta1pergunta12.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta12.setImagemresposta(R.drawable.banheiro);
            resposta1pergunta12.setCorreta("nao");
            respostasdao.insert(resposta1pergunta12);

            RespostasVO resposta2pergunta12 = new RespostasVO();
            resposta2pergunta12.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta12.setImagemresposta(R.drawable.carne);
            resposta2pergunta12.setCorreta("nao");
            respostasdao.insert(resposta2pergunta12);

            RespostasVO resposta3pergunta12 = new RespostasVO();
            resposta3pergunta12.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta12.setImagemresposta(R.drawable.feijao);
            resposta3pergunta12.setCorreta("sim");
            respostasdao.insert(resposta3pergunta12);

            //pergunta 13
            PerguntasVO pergunta13 = new PerguntasVO();
            pergunta13.setIdcategoria(categoriasdao.getCategoriaByName("Substantivos").getId());
            pergunta13.setPergunta("Como se diz TELEFONE em Libras?");
            idpergunta = perguntasdao.insert(pergunta13);

            RespostasVO resposta1pergunta13 = new RespostasVO();
            resposta1pergunta13.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta13.setImagemresposta(R.drawable.telefone);
            resposta1pergunta13.setCorreta("sim");
            respostasdao.insert(resposta1pergunta13);

            RespostasVO resposta2pergunta13 = new RespostasVO();
            resposta2pergunta13.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta13.setImagemresposta(R.drawable.prato);
            resposta2pergunta13.setCorreta("nao");
            respostasdao.insert(resposta2pergunta13);

            RespostasVO resposta3pergunta13 = new RespostasVO();
            resposta3pergunta13.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta13.setImagemresposta(R.drawable.agua);
            resposta3pergunta13.setCorreta("nao");
            respostasdao.insert(resposta3pergunta13);

            //pergunta 14
            PerguntasVO pergunta14 = new PerguntasVO();
            pergunta14.setIdcategoria(categoriasdao.getCategoriaByName("Alimentos").getId());
            pergunta14.setPergunta("Como se diz CARNE?");
            idpergunta = perguntasdao.insert(pergunta14);

            RespostasVO resposta1pergunta14 = new RespostasVO();
            resposta1pergunta14.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta14.setImagemresposta(R.drawable.pequeno);
            resposta1pergunta14.setCorreta("nao");
            respostasdao.insert(resposta1pergunta14);

            RespostasVO resposta2pergunta14 = new RespostasVO();
            resposta2pergunta14.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta14.setImagemresposta(R.drawable.carne);
            resposta2pergunta14.setCorreta("sim");
            respostasdao.insert(resposta2pergunta14);

            RespostasVO resposta3pergunta14 = new RespostasVO();
            resposta3pergunta14.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta14.setImagemresposta(R.drawable.peixe);
            resposta3pergunta14.setCorreta("nao");
            respostasdao.insert(resposta3pergunta14);

            //pergunta 15
            PerguntasVO pergunta15 = new PerguntasVO();
            pergunta15.setIdcategoria(categoriasdao.getCategoriaByName("Adjetivos").getId());
            pergunta15.setPergunta("Como se diz PEQUENO em Libras?");
            idpergunta = perguntasdao.insert(pergunta15);

            RespostasVO resposta1pergunta15 = new RespostasVO();
            resposta1pergunta15.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta1pergunta15.setImagemresposta(R.drawable.limpo);
            resposta1pergunta15.setCorreta("nao");
            respostasdao.insert(resposta1pergunta15);

            RespostasVO resposta2pergunta15 = new RespostasVO();
            resposta2pergunta15.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta2pergunta15.setImagemresposta(R.drawable.feijao);
            resposta2pergunta15.setCorreta("nao");
            respostasdao.insert(resposta2pergunta15);

            RespostasVO resposta3pergunta15 = new RespostasVO();
            resposta3pergunta15.setIdpergunta(Integer.valueOf((int) idpergunta));
            resposta3pergunta15.setImagemresposta(R.drawable.pequeno);
            resposta3pergunta15.setCorreta("sim");
            respostasdao.insert(resposta3pergunta15);

            listaPerguntas = perguntasdao.list(categoriaSelecionada.getId());

        }else{
            listaPerguntas = perguntasdao.list(categoriaSelecionada.getId());
        }


        //insere os objetos no layout
        listaPerguntasLL = (LinearLayout) findViewById(R.id.listaperguntas);
        perguntaTV = (TextView) findViewById(R.id.perguntaTV);
        respacertosTV = (TextView) findViewById(R.id.respacertos);
        resperrosTV = (TextView) findViewById(R.id.resperros);

        final Button desistirBT = (Button) findViewById(R.id.desistirBT);
        desistirBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                finish();
            }
        });

        exibePerguntas(indice);

    }

    public void exibePerguntas(int indice){

        listaPerguntasLL.removeAllViews();

        RespostasDAO respostasdao = new RespostasDAO(getApplicationContext());
        perguntaTV.setText(listaPerguntas.get(indice).getPergunta());
        final List<RespostasVO> todasRespostas = respostasdao.list(listaPerguntas.get(indice).getId());
        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.setMargins(0,20,0,0);
        ll.gravity = Gravity.CENTER;
        for(int e=0; e<todasRespostas.size(); e++){
            final int b = e;
            if(todasRespostas.get(e).getCorreta().equals("sim")){
                correto = todasRespostas.get(e).getId();
            }

            ImageView img = new ImageView(this);
            img.setImageResource(todasRespostas.get(e).getImagemresposta());
            img.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            verficaResposta(todasRespostas.get(b).getId(), respacertosTV, resperrosTV);
                        }
                    }
            );

            listaPerguntasLL.addView(img, ll);
        }
    }

    public void verficaResposta(int idresposta, TextView acertos, TextView erros){

        indice++;

        if(idresposta == correto){
            aplausos();
            int ac = Integer.parseInt(acertos.getText().toString());
            ac++;
            acertos.setText(ac+"");

            int duracao = Toast.LENGTH_LONG;
            Toast t = Toast.makeText(this, "Acertou!" , duracao);
            t.show();
            try{
                System.out.println(listaPerguntas.get(indice).getPergunta());
                exibePerguntas(indice);
            }catch (Exception e){
                m_chronometer.stop();
                Intent it = new Intent(getApplicationContext(), ResultadoActivity.class);
                it.putExtra("acertos", acertos.getText().toString());
                it.putExtra("erros", erros.getText().toString());
                it.putExtra("tempo", m_chronometer.getText().toString());
                it.putExtra("nome", nome);
                it.putExtra("categoria", categoria);
                startActivity(it);
            }


        }else{
            vibrar();
            int er = Integer.parseInt(erros.getText().toString());
            er++;
            erros.setText(er+"");

            int duracao = Toast.LENGTH_LONG;
            Toast t = Toast.makeText(this, "Errou!" , duracao);
            t.show();
            try{
                System.out.println(listaPerguntas.get(indice).getPergunta());
                exibePerguntas(indice);
            }catch (Exception e){
                m_chronometer.stop();
                Intent it = new Intent(getApplicationContext(), ResultadoActivity.class);
                it.putExtra("acertos", acertos.getText().toString());
                it.putExtra("erros", erros.getText().toString());
                it.putExtra("tempo", m_chronometer.getText().toString());
                it.putExtra("nome", nome);
                it.putExtra("categoria", categoria);
                startActivity(it);
            }
        }
    }

    private void vibrar()
    {
        Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long milliseconds = 1000;//'30' é o tempo em milissegundos, é basicamente o tempo de duração da vibração. portanto, quanto maior este numero, mais tempo de vibração você irá ter
        rr.vibrate(milliseconds);
    }
    private void aplausos(){
        MediaPlayer mp = MediaPlayer.create(this, R.raw.aplausos);
        mp.start();
    }
    public static PerguntasActivity getInstance(){
        return activity;
    }

}
