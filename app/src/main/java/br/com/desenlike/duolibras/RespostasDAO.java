package br.com.desenlike.duolibras;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erick on 31/08/2016.
 */
public class RespostasDAO {
    private Context ctx;
    private String table_name = "respostas";
    private String[] colunas = new String[] { "id", "idpergunta", "imagemresposta", "correta" };

    public RespostasDAO(Context ctx){
        this.ctx = ctx;
    }

    public long insert(RespostasVO resposta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idpergunta", resposta.getIdpergunta());
        values.put("imagemresposta", resposta.getImagemresposta());
        values.put("correta", resposta.getCorreta());
        return db.insert(table_name, null, values);
    }

    public boolean update(RespostasVO resposta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idpergunta", resposta.getIdpergunta());
        values.put("imagemresposta", resposta.getImagemresposta());
        values.put("correta", resposta.getCorreta());

        if(db.update(table_name, values, "id = '" + resposta.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public boolean delete(RespostasVO resposta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        if(db.delete(table_name, "id = '" + resposta.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public List<RespostasVO> list(int idpergunta) {

        List<RespostasVO> lista = new ArrayList<RespostasVO>();

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, "idpergunta = '"+ idpergunta + "'", null, null, null, null);

        while (c.moveToNext()) {
            RespostasVO resposta = new RespostasVO();
            resposta.setId(c.getInt(c.getColumnIndex("id")));
            resposta.setIdpergunta(c.getInt(c.getColumnIndex("idpergunta")));
            resposta.setImagemresposta(c.getInt(c.getColumnIndex("imagemresposta")));
            resposta.setCorreta(c.getString(c.getColumnIndex("correta")));
            lista.add(resposta);
        }
        c.close();
        return lista;
    }
}
