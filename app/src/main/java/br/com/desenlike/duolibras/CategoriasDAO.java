package br.com.desenlike.duolibras;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erick on 22/11/2016.
 */
public class CategoriasDAO {

    private Context ctx;
    private String table_name = "categorias";
    private String[] colunas = new String[] { "id", "nome" };

    public CategoriasDAO(Context ctx){
        this.ctx = ctx;
    }

    public long insert(CategoriasVO categoria) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nome", categoria.getNome());
        return db.insert(table_name, null, values);
    }

    public boolean update(CategoriasVO categoria) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nome", categoria.getNome());

        if(db.update(table_name, values, "id = '" + categoria.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public boolean delete(CategoriasVO categoria) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        if(db.delete(table_name, "id = '" + categoria.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public List<CategoriasVO> list() {

        List<CategoriasVO> lista = new ArrayList<CategoriasVO>();

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, null, null, null, null, null);

        while (c.moveToNext()) {
            CategoriasVO categoria = new CategoriasVO(c.getString(c.getColumnIndex("nome")));
            categoria.setId(c.getInt(c.getColumnIndex("id")));
            lista.add(categoria);
        }
        c.close();
        return lista;
    }

    public CategoriasVO getCategoriaByName(String nome) {

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, "nome = '"+nome+"'", null, null, null, null);

        CategoriasVO categoria = null;
        while (c.moveToNext()) {
            categoria = new CategoriasVO(c.getString(c.getColumnIndex("nome")));
            categoria.setId(c.getInt(c.getColumnIndex("id")));
        }
        c.close();
        return categoria;
    }

    public String[] listArray( List<CategoriasVO> lista ) {

        String[] listaArray = new String[lista.size()];

        for(int i=0; i<lista.size();i++){
            listaArray[i] = lista.get(i).getNome();
        }

        return listaArray;
    }
}
