package br.com.desenlike.duolibras;

/**
 * Created by erick on 22/11/2016.
 */
public class CategoriasVO {
    int id;
    String nome;

    public CategoriasVO(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
