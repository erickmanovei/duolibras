package br.com.desenlike.duolibras;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

/**
 * Created by erick on 08/09/2016.
 */
public class HistoricoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historicolistview);

        HistoricoDAO dao = new HistoricoDAO(this);

        List<ItemListView> historicos = dao.listListView();
        ListView listaDeCursos = (ListView) findViewById(R.id.list);
        AdapterListView adapter = new AdapterListView(this, historicos);
        listaDeCursos.setAdapter(adapter);

    }
}
