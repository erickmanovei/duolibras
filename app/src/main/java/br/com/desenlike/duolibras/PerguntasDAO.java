package br.com.desenlike.duolibras;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erick on 31/08/2016.
 */
public class PerguntasDAO {
    private Context ctx;
    private String table_name = "perguntas";
    private String[] colunas = new String[] { "id", "idcategoria", "pergunta" };

    public PerguntasDAO(Context ctx){
        this.ctx = ctx;
    }

    public long insert(PerguntasVO pergunta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idcategoria", pergunta.getIdcategoria());
        values.put("pergunta", pergunta.getPergunta());
        return db.insert(table_name, null, values);
    }

    public boolean update(PerguntasVO pergunta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idcategoria", pergunta.getIdcategoria());
        values.put("pergunta", pergunta.getPergunta());

        if(db.update(table_name, values, "id = '" + pergunta.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public boolean delete(PerguntasVO pergunta) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        if(db.delete(table_name, "id = '" + pergunta.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public List<PerguntasVO> list(int idcategoria) {

        List<PerguntasVO> lista = new ArrayList<PerguntasVO>();

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, "idcategoria ='"+idcategoria+"'", null, null, null, null);

        while (c.moveToNext()) {
            PerguntasVO pergunta = new PerguntasVO();
            pergunta.setId(c.getInt(c.getColumnIndex("id")));
            pergunta.setIdcategoria(c.getInt(c.getColumnIndex("idcategoria")));
            pergunta.setPergunta(c.getString(c.getColumnIndex("pergunta")));
            lista.add(pergunta);
        }
        c.close();
        return lista;
    }
}
