package br.com.desenlike.duolibras;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by erick on 31/08/2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "duolibrasdompedro.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS categorias (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome VARCHAR(50) NOT NULL)");
        db.execSQL("CREATE TABLE IF NOT EXISTS perguntas (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, idcategoria INTEGER NOT NULL, pergunta VARCHAR(255) NOT NULL)");
        db.execSQL("CREATE TABLE IF NOT EXISTS respostas (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, idpergunta INTEGER NOT NULL, imagemresposta INTEGER NOT NULL, correta VARCHAR(3) NOT NULL)");
        db.execSQL("CREATE TABLE IF NOT EXISTS historico (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome VARCHAR(50) NOT NULL, acertos INTEGER, erros INTEGER, tempo VARCHAR(25) NOT NULL, datahora TEXT, categoria VARCHAR(50) NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion == 1){
            if (newVersion == 2){

            }
        }
    }
}
