package br.com.desenlike.duolibras;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        final Button iniciarSimuladoBT = (Button) findViewById(R.id.iniciarSimuladoBT);
        iniciarSimuladoBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                abrirActivity(1);
            }
        });

        final Button sobreBT = (Button) findViewById(R.id.sobreBT);
        sobreBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                abrirActivity(2);
            }
        });

        final Button historicoBT = (Button) findViewById(R.id.historico);
        historicoBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                abrirActivity(3);
            }
        });

        final Button sairBT = (Button) findViewById(R.id.sairBT);
        sairBT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });


    }


    public void abrirActivity(int menu){

        Intent it = null;
        switch (menu){
            case 1:
                //cadastra as categorias
                CategoriasDAO categoriasdao = new CategoriasDAO(this);

                try {
                    List<CategoriasVO> lst = categoriasdao.list();
                    if(lst.size() < 1) {

                        //categoria1
                        CategoriasVO catsubstantivo = new CategoriasVO("Substantivos");
                        categoriasdao.insert(catsubstantivo);

                        //categoria2
                        CategoriasVO catsaudacoes = new CategoriasVO("Saudações");
                        categoriasdao.insert(catsaudacoes);

                        //categoria3
                        CategoriasVO catverbos = new CategoriasVO("Verbos");
                        categoriasdao.insert(catverbos);

                        //categoria4
                        CategoriasVO catanimais = new CategoriasVO("Animais");
                        categoriasdao.insert(catanimais);

                        //categoria5
                        CategoriasVO catveiculos = new CategoriasVO("Veículos");
                        categoriasdao.insert(catveiculos);

                        //categoria6
                        CategoriasVO catalimentos = new CategoriasVO("Alimentos");
                        categoriasdao.insert(catalimentos);

                        //categoria7
                        CategoriasVO catadjetivos = new CategoriasVO("Adjetivos");
                        categoriasdao.insert(catadjetivos);
                    }
                    solicitaNome("Vamos Iniciar!", "Preencha as Informações Abaixo");

                }catch (Exception e){
                    System.out.println("Erro: "+e.getMessage());
                }

                break;
            case 2:
                it = new Intent(getApplicationContext(), SobreActivity.class);
                startActivity(it);
                break;
            case 3:
                it = new Intent(getApplicationContext(), HistoricoActivity.class);
                startActivity(it);
                break;
        }

    }

    public void solicitaNome(String titulo, String texto){

        AlertDialog.Builder mensagem = new AlertDialog.Builder(this);
        mensagem.setTitle(titulo);
        mensagem.setMessage(texto);


        // DECLARACAO DO EDITTEXT
        final EditText input = new EditText(this);

        //declaração do textview
        final TextView tvNome = new TextView(this);
        tvNome.setText("Seu Nome:");

        //declaração do textview
        final TextView tvCategoria = new TextView(this);
        tvCategoria.setText("Categoria Desejada:");

        //pega as categorias e insere no spinner
        CategoriasDAO dao = new CategoriasDAO(getApplicationContext());

        String[] categoriasArray = dao.listArray(dao.list());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, categoriasArray);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        final Spinner spinner = new Spinner(this);
        spinner.setAdapter(adapter);


        final LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(tvNome);
        ll.addView(input);
        ll.addView(tvCategoria);
        ll.addView(spinner);

        mensagem.setView(ll);


        mensagem.setNeutralButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                Intent it = new Intent(getApplicationContext(), PerguntasActivity.class);
                it.putExtra("nome", input.getText().toString());
                it.putExtra("categoria", spinner.getSelectedItem().toString());
                startActivity(it);
            }

        });

        mensagem.show();
        // FORÇA O TECLADO APARECER AO ABRIR O ALERT
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
