package br.com.desenlike.duolibras;

/**
 * Created by erick on 31/08/2016.
 */
public class RespostasVO {

    int id;
    int idpergunta;
    int imagemresposta;
    String correta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdpergunta() {
        return idpergunta;
    }

    public void setIdpergunta(int idpergunta) {
        this.idpergunta = idpergunta;
    }

    public int getImagemresposta() {
        return imagemresposta;
    }

    public void setImagemresposta(int imagemresposta) {
        this.imagemresposta = imagemresposta;
    }

    public String getCorreta() {
        return correta;
    }

    public void setCorreta(String correta) {
        this.correta = correta;
    }
}
