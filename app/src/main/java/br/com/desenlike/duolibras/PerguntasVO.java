package br.com.desenlike.duolibras;

/**
 * Created by erick on 31/08/2016.
 */
public class PerguntasVO {
    private int id;
    private int idcategoria;
    private String pergunta;

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }
}
