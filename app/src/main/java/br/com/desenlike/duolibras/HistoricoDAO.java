package br.com.desenlike.duolibras;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erick on 06/09/2016.
 */
public class HistoricoDAO {
    private Context ctx;
    private String table_name = "historico";
    private String[] colunas = new String[] { "id", "nome", "acertos", "erros", "tempo", "datahora", "categoria" };

    public HistoricoDAO(Context ctx){
        this.ctx = ctx;
    }

    public long insert(HistoricoVO historico) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nome", historico.getNome());
        values.put("acertos", historico.getAcertos());
        values.put("erros", historico.getErros());
        values.put("tempo", historico.getTempo());
        values.put("datahora", historico.getDatahora());
        values.put("categoria", historico.getCategoria());
        return db.insert(table_name, null, values);
    }

    public boolean update(HistoricoVO historico) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nome", historico.getNome());
        values.put("acertos", historico.getAcertos());
        values.put("erros", historico.getErros());
        values.put("tempo", historico.getTempo());
        values.put("datahora", historico.getDatahora());
        values.put("categoria", historico.getCategoria());

        if(db.update(table_name, values, "id = '" + historico.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public boolean delete(HistoricoVO historico) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        if(db.delete(table_name, "id = '" + historico.getId() + "'", null) > 0){
            return true;
        }else{
            return false;
        }

    }

    public List<HistoricoVO> list() {

        List<HistoricoVO> lista = new ArrayList<HistoricoVO>();

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, null, null, null, null, null);

        while (c.moveToNext()) {
            HistoricoVO historico = new HistoricoVO();
            historico.setId(c.getInt(c.getColumnIndex("id")));
            historico.setNome(c.getString(c.getColumnIndex("nome")));
            historico.setAcertos(c.getInt(c.getColumnIndex("acertos")));
            historico.setErros(c.getInt(c.getColumnIndex("erros")));
            historico.setTempo(c.getString(c.getColumnIndex("tempo")));
            historico.setDatahora(c.getString(c.getColumnIndex("datahora")));
            historico.setCategoria(c.getString(c.getColumnIndex("categoria")));
            lista.add(historico);
        }
        c.close();
        return lista;
    }

    public List<ItemListView> listListView() {

        List<ItemListView> lista = new ArrayList<ItemListView>();

        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        Cursor c = db.query(table_name, colunas, null, null, null, null, null);

        while (c.moveToNext()) {
            ItemListView historico = new ItemListView(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("nome")),
                    c.getInt(c.getColumnIndex("acertos")),
                    c.getInt(c.getColumnIndex("erros")),
                    c.getString(c.getColumnIndex("tempo")),
                    c.getString(c.getColumnIndex("datahora")),
                    c.getString(c.getColumnIndex("categoria"))
            );
            lista.add(historico);
        }
        c.close();
        return lista;
    }
}
