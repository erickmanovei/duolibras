package br.com.desenlike.duolibras;

/**
 * Created by erick on 08/09/2016.
 */
public class ItemListView {
    int id;
    String nome;
    int acertos;
    int erros;
    String tempo;
    String datahora;
    String categoria;



    public ItemListView(int id, String nome, int acertos, int erros, String tempo, String datahora, String categoria) {
        this.id = id;
        this.nome = nome;
        this.acertos = acertos;
        this.erros = erros;
        this.tempo = tempo;
        this.datahora = datahora;
        this.categoria = categoria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAcertos() {
        return acertos;
    }

    public void setAcertos(int acertos) {
        this.acertos = acertos;
    }

    public int getErros() {
        return erros;
    }

    public void setErros(int erros) {
        this.erros = erros;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getDatahora() {
        return datahora;
    }

    public void setDatahora(String datahora) {
        this.datahora = datahora;
    }
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
